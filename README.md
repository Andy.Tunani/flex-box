# Flex-box

**justify-content:** 
- flex-start : Les éléments s'alignent au côté gauche du conteneur.
- flex-end : Les éléments s'alignent au côté droit du conteneur.
- center : Les éléments s'alignent au centre du conteneur.
- space-between : Les éléments s'affichent avec un espace égal entre eux.
- space-around : Les éléments s'affichent avec un espacement égal à 
  l'entour d'eux.


**align-items:**

- flex-start : Les éléments s'alignent au haut du conteneur.
- flex-end : Les éléments s'alignent au bas du conteneur.
- center : Les éléments s'alignent au centre vertical du conteneur.
- baseline : Les éléments s'alignent à la ligne de base du conteneur.
- stretch : Les éléments sont étirés pour s'adapter au conteneur.


**flex-direction:** - Remarquez que quand vous mettez 
la direction inversée, le début et la fin sont aussi inversés.

- row : Les éléments sont disposés dans la même direction que le texte.
- row-reverse : Les éléments sont disposés dans la direction opposée au texte.
- column : Les éléments sont disposés de haut en bas.
- column-reverse : Les éléments sont disposés de bas en haut.


**order:** Parfois, inverser l'ordre de la rangée ou la colonne ne suffit pas.
Dans ces cas, on peut appliquer la propriété order à des éléments individuels.
Par défaut, les éléments ont une valeur de 0, mais on peut utiliser 
cette propriété pour changer la valeur à un entier positif ou négatif.




**align-self:** Une autre propriété que vous pouvez appliquer
sur des éléments individuels est align-self.
Cette propriété accepte les mêmes valeurs que align-items,
mais s'applique seulement à l'élément ciblé.

- nowrap : Tous les éléments sont tenus sur une seule ligne.
- wrap : Les éléments s'enveloppent sur plusieurs lignes au besoin.
- wrap-reverse : Les éléments s'enveloppent sur plusieurs lignes 
  dans l'ordre inversé.




**flex-wrap:** 
- nowrap : Tous les éléments sont tenus sur une seule ligne.

- wrap : Les éléments s'enveloppent sur plusieurs lignes au besoin.

- wrap-reverse : Les éléments s'enveloppent sur plusieurs lignes dans 
  l'ordre inversé.

**flex-flow**: Les deux propriétés flex-direction et flex-wrap sont utilisées 
tellement souvent ensembles que le raccourci flex-flow a été créé pour 
les combiner. 

Ce raccourci accepte les valeurs des deux propriétés séparées par un espace.

Par exemple, vous pouvez utiliser "_flex-flow: 
row wrap_" pour configurer les colonnes et les faire s'envelopper.


**align-content:** pour définir comment plusieurs lignes sont
espacées de l'une à l'autre.

- flex-start : Les lignes sont amassées dans le haut du conteneur.
- flex-end: Les lignes sont amassées dans le bas du conteneur.
- center : Les lignes sont amassées dans le centre vertical du conteneur.
- space-between : Les lignes s'affichent avec un espace égal entre eux.
- space-around : Les lignes s'affichent avec un espace égal autour d'eux.
- stretch : Les lignes sont étirées pour s'adapter au conteneur.

"align-content" détermine l'espace entre les lignes, 
alors que align-items détermine comment les éléments 
dans leur ensemble sont alignées à l'intérieur du conteneur. 
Quand il n'y a qu'une seule ligne, align-content n'a aucun effet.

















